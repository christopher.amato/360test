//
//  Copyright (c) SRG SSR. All rights reserved.
//
//  License information is available from the LICENSE file.
//

#import "SRGMediaPlayerConstants.h"

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/**
 *  Lightweight navigation controller subclass forwarding status bar appearance from its top child view controller.
 */
@interface SRGMediaPlayerNavigationController : UINavigationController

@end

NS_ASSUME_NONNULL_END
