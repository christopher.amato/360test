//
//  Copyright (c) SRG SSR. All rights reserved.
//
//  License information is available from the LICENSE file.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSBundle (SRGLogger)

/**
 *  The logger resource bundle
 */
@property (class, nonatomic, readonly) NSBundle *srg_loggerBundle;

@end

NS_ASSUME_NONNULL_END
