//
//  ViewController.swift
//  360test
//
//  Created by Christopher Amato on 18/09/2019.
//  Copyright © 2019 Christo Apps. All rights reserved.
//

import UIKit
import SRGMediaPlayer
import Firebase

let tests = [ "https://rtsvodww-vh.akamaihd.net/i/360/2017/2_Gothard_360_full_f_8414077-,301k,701k,1201k,2001k,.mp4.csmil/master.m3u8", "https://bitmovin.com/player-content/playhouse-vr/m3u8s/105560.m3u8" ]

let demo = "https://cdn3.wowza.com/1/M3NQN09KQkh3ZG1j/NGZWejFV/hls/live/playlist.m3u8"

class ViewController: UIViewController {
    
    @IBOutlet var d3Button: UIButton!
    var count = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func dTapped(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func testTapped() {
        

        if let url = URL(string: tests[count]) {
//        if let url = Bundle.main.url(forResource: "test", withExtension:"mp4") {
            let vc = SRGMediaPlayerViewController()
            vc.controller.view?.viewMode = d3Button.isSelected ? .monoscopic : .flat
            vc.modalPresentationStyle = .fullScreen
            vc.controller.play(url)
            vc.controller.mediaConfigurationBlock = {(avItem, avAsset) in
                print("mediaConfigurationBlock")
                print("Resolution: \(avItem.preferredPeakBitRate)")
                print("DVR: \(vc.controller.streamType == SRGMediaPlayerStreamType.DVR), live: \(vc.controller.streamType == SRGMediaPlayerStreamType.live), onDemand: \(vc.controller.streamType == SRGMediaPlayerStreamType.onDemand), Raw: \(vc.controller.streamType.rawValue)")
                print("Live: \(vc.controller.isLive)")
                print("Live: \(String(describing: vc.controller.date))")
            }
            present(vc, animated: true) { //[weak self] in
//                print("present")
//                print("Resolution: \(vc.controller.player?.currentItem?.preferredMaximumResolution)")
//                print("DVR: \(vc.controller.streamType == SRGMediaPlayerStreamType.DVR), live: \(vc.controller.streamType == SRGMediaPlayerStreamType.live), onDemand: \(vc.controller.streamType == SRGMediaPlayerStreamType.onDemand), Raw: \(vc.controller.streamType.rawValue)")
//                print("Live: \(vc.controller.isLive)")
//                print("Live: \(String(describing: vc.controller.date))")
            }
        }
        count += 1
        if count >= tests.count {count = 0}
    }
    
    @IBAction func demoTapped() {
        
        if let url = URL(string: demo) {
            let vc = SRGMediaPlayerViewController()
            vc.controller.view?.viewMode = d3Button.isSelected ? .monoscopic : .flat
            vc.modalPresentationStyle = .fullScreen
            vc.controller.play(url)
            vc.controller.mediaConfigurationBlock = {(avItem, avAsset) in
                print("mediaConfigurationBlock")
                print("Resolution: \(avItem.preferredPeakBitRate)")
                print("DVR: \(vc.controller.streamType == SRGMediaPlayerStreamType.DVR), live: \(vc.controller.streamType == SRGMediaPlayerStreamType.live), onDemand: \(vc.controller.streamType == SRGMediaPlayerStreamType.onDemand), Raw: \(vc.controller.streamType.rawValue)")
                print("Live: \(vc.controller.isLive)")
                print("Live: \(String(describing: vc.controller.date))")
            }
            present(vc, animated: true) { //[weak self] in
//                print("present")
//                print("Resolution: \(vc.controller.player?.currentItem?.preferredMaximumResolution)")
//                print("DVR: \(vc.controller.streamType == SRGMediaPlayerStreamType.DVR), live: \(vc.controller.streamType == SRGMediaPlayerStreamType.live), onDemand: \(vc.controller.streamType == SRGMediaPlayerStreamType.onDemand), Raw: \(vc.controller.streamType.rawValue)")
//                print("Live: \(vc.controller.isLive)")
//                print("Live: \(String(describing: vc.controller.date))")
            }
        }
    }
    
}

